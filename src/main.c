#include <sys/types.h>  
#include <sys/stat.h>
#include <fcntl.h>
 
#include "capture_and_encoding.h"

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>  //Header file for sleep(). man 3 sleep for details.
#include <pthread.h>
#include "rtmpStream.h"

RTMP *rtmp = NULL;	
RTMPMetadata MetaData;

// #define RTMP_SERVER "rtmp://192.168.1.76:1935/live/stream"
// #define RTMP_SERVER "rtmp://a.rtmp.youtube.com/live2/u0t3-2cct-zjsj-0k9s-3ha8"
#define RTMP_SERVER "rtmp://media-01.beex.vn:1935/PVNTEST1234567890?user=admin&pass=0qT1ojcd0CTV1j1uEiUuUX227GHHn594pJQUfEpH1p1D7e"
// #define RTMP_SERVER "rtmp://192.168.1.88/live/stream"

#define F_SETPIPE_SZ 1024 * 64
#define VIDEO_CAPTURE_FIFO_SIZE 160000	//(64000*2)

char const* inputVideoFileName = "/tmp/capture_fifo";;

void *icam_video_capture(void *arg);

int main(int argc, char** argv) {

	rtmp = AllocRtmp();	
	if(rtmp==NULL)	
	{		
		printf("rtmp alloc failed\n");
		return -1;
	}	
	printf("Connected\n");
	if (ConnectToServer(rtmp, RTMP_SERVER) != 0){
		printf("Connect RTMP server fail\n");
	}

	// if (ConnectToServer(rtmp, "rtmp://a.rtmp.youtube.com/live2/u0t3-2cct-zjsj-0k9s-3ha8") != 0){
	// 	printf("Connect RTMP server fail\n");
	// }

	// if (ConnectToServer(rtmp, "rtmp://192.168.1.76:1935/live/livestream") != 0){
	// 	printf("Connect RTMP server fail\n");
	// }

	// rtmp://localhost/live/livestream
	memset(&MetaData, 0, sizeof(RTMPMetadata));
	printf("SendH264File\n");

	// SendH264File(rtmp, &MetaData, "/tmp/SD/ICAM_RECORD/VIDEO_RECORD/pvn_video_1650334648_29.h264");
	// SendH264File(rtmp, &MetaData, "h264.h264");
	printf("SendH264File11\n");

	pthread_t thread_info;
	pthread_attr_t thread_attr;
	unsigned int stacksize;

	pthread_attr_init(&thread_attr);
	pthread_attr_getstacksize (&thread_attr, &stacksize);
	stacksize = stacksize/2;
	pthread_attr_setstacksize (&thread_attr, stacksize);

	pthread_create(&thread_info, &thread_attr, icam_video_capture, NULL);

	int cnt = 500;
	while (cnt--)
	{
		sleep(1);
	}

	DisconnectToServer();
	return 0; // only to prevent compiler warning
}

void *icam_video_capture(void *arg)
{
	int fd;

	//基于君正提供的API初始化实现采集和编码模块
	if (capture_and_encoding() != 0) {
		printf ("unable to setup camera stream\n");
		exit(1);
	}

	unlink(inputVideoFileName);

	if (mkfifo(inputVideoFileName, 0777) < 0) {
			  printf ("mkfifo Failed\n");
			  exit(1);; 
	}

	fd = open(inputVideoFileName, O_RDWR | O_CREAT | O_TRUNC, 0777);
	if (fd < 0) {
			printf ("Failed open fifo\n");
			exit(1);; 
	}
	fcntl(fd, F_SETPIPE_SZ, VIDEO_CAPTURE_FIFO_SIZE);
	while (1) {
			if (get_stream(fd ,0) < 0) break; //基于君正提供的API实现采集和编码，并将编码后的数据保存到fifo中。
	}
	return NULL;
}