/*ringbuf .c*/

#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include "ringfifo.h"

#define MAX_RING 2
#define NMAX 20//32

#if 0
int iput = 0;			/* The current placement of the ring buffer */
int iget = 0;			/* Current fetch location of the buffer */
int n = 0;				/* Total number of elements in the ring buffer */

struct ringbuf ringfifo[NMAX];
#endif

typedef struct
{
	int iput;			/* The current placement of the ring buffer */
	int iget;			/* Current fetch location of the buffer */
	int n;				/* Total number of elements in the ring buffer */
	struct ringbuf ringfifo[NMAX];
}ring_data_t;

ring_data_t ring_data[MAX_RING];

pthread_mutex_t ringfifo_mutex[MAX_RING];

void ringmalloc(int index)
{
	int i;
	for (i = 0; i < NMAX; i++) {
		ring_data[index].ringfifo[i].size = 0;
		ring_data[index].ringfifo[i].channel = 0;
		//printf("FIFO INFO:idx:%d,len:%d,ptr:%x\n",i,ringfifo[i].size,(int)(ringfifo[i].buffer));
	}
	ring_data[index].iput = 0;		/* The current placement of the ring buffer */
	ring_data[index].iget = 0;		/* Current fetch location of the buffer */
	ring_data[index].n = 0;			/* Total number of elements in the ring buffer */
	pthread_mutex_init(&ringfifo_mutex[index], NULL);
}

void ringreset(int i)
{
	ring_data[i].iput = 0;		/* The current placement of the ring buffer */
	ring_data[i].iget = 0;		/* Current fetch location of the buffer */
	ring_data[i].n = 0;			/* Total number of elements in the ring buffer */
}

void ringfree(int index)
{
	int i;
	printf("begin free mem");
	for (i = 0; i < NMAX; i++) {
		//printf("FREE FIFO INFO:idx:%d,len:%d,ptr:%x\n",i,ringfifo[i].size,(int)(ringfifo[i].buffer));
		if(ring_data[index].ringfifo[i].buffer){
			free(ring_data[index].ringfifo[i].buffer);//haivn
		}
		ring_data[index].ringfifo[i].size = 0;
	}
}

int addring(int i)
{
	return (i + 1) == NMAX ? 0 : i + 1;
}

/* Take an element from the ring buffer */
int ringget(int i, struct ringbuf *getinfo)
{
	int pos = 0;
	int size = 0;

	pthread_mutex_lock(&ringfifo_mutex[i]);
	if (ring_data[i].n > 0) {
		pos = ring_data[i].iget;
		ring_data[i].iget = addring(pos);
		ring_data[i].n--;
		/* If you don't know the previous size (you don't know wether you're scaling down or up), use malloc()/free().
		When scaling down, realloc() is ~40 times faster, but when scaling up, realloc() is ~7600 times slower.*/
		if (getinfo->buffer)
			free(getinfo->buffer);
		getinfo->buffer = (unsigned char*)malloc(ring_data[i].ringfifo[pos].size);
		if (getinfo->buffer == NULL) {
			printf("ringget: Can not malloc for getinfo buffer\n");
			size = 0;
		}
		else
		{
			memcpy(getinfo->buffer, ring_data[i].ringfifo[pos].buffer, ring_data[i].ringfifo[pos].size);
			getinfo->size = ring_data[i].ringfifo[pos].size;
			getinfo->channel = ring_data[i].ringfifo[pos].channel;
			getinfo->timestamp = ring_data[i].ringfifo[pos].timestamp;

			size = ring_data[i].ringfifo[pos].size;

			if(ring_data[i].ringfifo[pos].buffer){
				free(ring_data[i].ringfifo[pos].buffer);
				ring_data[i].ringfifo[pos].buffer = NULL;
			}
		}
	} else {
		// printf("Buffer is empty: %d\n", i);
		size = 0;
	}
	pthread_mutex_unlock(&ringfifo_mutex[i]);
	return size;
}

/* Put an element into the ring buffer */
int ringput(int i, unsigned char *buffer, int size, int channel, uint64_t timestamp)
{
	int ret = 0;
	int put = 0;
	pthread_mutex_lock(&ringfifo_mutex[i]);
	if (ring_data[i].n < NMAX) {

		put = ring_data[i].iput;
		ring_data[i].ringfifo[put].buffer = (unsigned char*)malloc(size);
		if (ring_data[i].ringfifo[put].buffer) {
			memcpy(ring_data[i].ringfifo[put].buffer, buffer, size);
			ring_data[i].ringfifo[put].size = size;
			ring_data[i].ringfifo[put].channel = channel;
			ring_data[i].ringfifo[put].timestamp = timestamp;
			//printf("Put FIFO INFO:idx:%d,len:%d,ptr:%x\n",iput,ringfifo[iput].size,(int)(ringfifo[iput].buffer));
			ring_data[i].iput = addring(put);
			ring_data[i].n++;
		} else {
			printf("ringput: can not allocate memory for ringfifo %d\n", put);
		}
		//printf("put Video ok n=[%d] input=[%d]", n, iput);
	} else {
			// printf("Buffer is full: %d\n", i);
		ret = -1;
	}
	pthread_mutex_unlock(&ringfifo_mutex[i]);
	return ret;
}

int ringputptr(int i, unsigned char *buffer, int channel)
{
	int ret = 0;
	pthread_mutex_lock(&ringfifo_mutex[i]);
	if (ring_data[i].n < NMAX) {
		ring_data[i].ringfifo[ring_data[i].iput].buffer = buffer;
		ring_data[i].ringfifo[ring_data[i].iput].size = -1;
		ring_data[i].ringfifo[ring_data[i].iput].channel = channel;
		//printf("Put FIFO INFO:idx:%d,len:%d,ptr:%x\n",iput,ringfifo[iput].size,(int)(ringfifo[iput].buffer));
		ring_data[i].iput = addring(ring_data[i].iput);
		ring_data[i].n++;
		//printf("put Video ok n=[%d] input=[%d]", n, iput);
	} else {
		printf("Buffer is full\n");
		ret = -1;
	}
	pthread_mutex_unlock(&ringfifo_mutex[i]);
	return ret;
}
