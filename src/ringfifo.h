#ifndef RING_BUF_H
#define RING_BUF_H
#include <stdint.h>
struct ringbuf {
	unsigned char *buffer;
	int channel;
	int size;
	int64_t	timestamp;
};
int addring(int i);
int ringget(int i, struct ringbuf *getinfo);
int ringput(int i, unsigned char *buffer, int size, int channel, uint64_t timestamp);
int ringputptr(int i, unsigned char *buffer, int channel);
void ringfree(int i);
void ringmalloc(int i);
void ringreset(int i);
#endif
