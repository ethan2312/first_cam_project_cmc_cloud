#include "log.h"
#include "rtmpStream.h"
#include <pthread.h>
#include <stdbool.h>

static pthread_mutex_t rtmp_sync = PTHREAD_MUTEX_INITIALIZER;

RTMP *g_rtmp = NULL;
RTMPMetadata *MetaData = NULL;

uint32_t tick_video = 0;
uint32_t tick_audio = 0;
uint32_t tick_gap = 0;

typedef struct
{
	unsigned char channel_conf_3        :1;
	unsigned char private_stream        :1;
	unsigned char sample_freqency_index :4;
	unsigned char profile               :2;
}ADTS_2;

typedef struct
{
	unsigned char framelength_13to12    :2;
	unsigned char copyright_start       :1;
	unsigned char copyrighted_stream    :1;
	unsigned char home                  :1;
	unsigned char originality           :1;
	unsigned char channel_conf_2to1     :2;
}ADTS_3;

RTMP * AllocRtmp()
{
	RTMP *rtmp=NULL;
	rtmp = RTMP_Alloc();
	if (rtmp == NULL)
		return NULL;
	RTMP_Init(rtmp);

	rtmp->Link.timeout = 10;
	printf ("RTMP timeout connection: %d\n", rtmp->Link.timeout);
	return rtmp;
}

int ConnectToServer(RTMP *rtmp, char *url)
{
	if (RTMP_SetupURL(rtmp, url) == 0)
	{
		return -1;
	}
	RTMP_EnableWrite(rtmp);
	if (RTMP_Connect(rtmp, NULL) == 0)
	{
		return -1;
	}
	if (RTMP_ConnectStream(rtmp, 0) == 0)
	{
		RTMP_Close(rtmp);
		return -1;
	}

	// RTMP_LogSetLevel(RTMP_LOGDEBUG);

	return 0;
}

int rtmp_connect_to_server(char *rtmp_server)
{
	tick_gap = 0;
	tick_video = 0;
	tick_audio = 0;

	if(g_rtmp == NULL){
		printf("rtmp alloc\n");
		g_rtmp = AllocRtmp();
		if(g_rtmp == NULL){
			printf("rtmp alloc failed\n");
			return -1;
		}
	}

	if (ConnectToServer(g_rtmp, rtmp_server) != 0){
		printf("RTMP connect fail\n");
		return -1;
	}else{
		printf("RTMP connect success\n");
	}

#if 0
	if (MetaData != NULL) {
		if(MetaData->Pps != NULL){
			printf("Free Pps\n");
			free(MetaData->Pps);
			MetaData->Pps = NULL;
		}

		if(MetaData->Vps != NULL){
			printf("Free Vps\n");
			free(MetaData->Vps);
			MetaData->Vps = NULL;
		}

		if(MetaData->Sps != NULL){
			printf("Free Sps\n");
			free(MetaData->Sps);
			MetaData->Sps = NULL;
		}
		printf("Free MetaData\n");
		free(MetaData);
	}
	MetaData = malloc(sizeof(RTMPMetadata));
	if (MetaData == NULL)
	{
		printf("Can not malloc for Metadata\n");
		return -1;
	}
	memset(MetaData, 0, sizeof(RTMPMetadata));
#endif

	MetaData->is_send_header_aac = false;
	MetaData->is_send_header_h264 = false;
	return 0;
}

void DisconnectToServer(void)
{
	printf("Enter funtion DisconnectToServer\n");

	if(g_rtmp){
		if (RTMP_IsConnected(g_rtmp)){
			RTMP_DeleteStream(g_rtmp);
		}
		if (g_rtmp->Link.pubUser.av_val)
			free(g_rtmp->Link.pubUser.av_val);
		if (g_rtmp->Link.pubPasswd.av_val)
			free(g_rtmp->Link.pubPasswd.av_val);
		//this closes the socket if not already done
		RTMP_Close(g_rtmp);
		g_rtmp = NULL;
	}

#if 0
	if (MetaData != NULL) {
		if(MetaData->Pps != NULL){
			printf("Free Pps\n");
			free(MetaData->Pps);
			MetaData->Pps = NULL;
		}

		if(MetaData->Vps != NULL){
			printf("Free Vps\n");
			free(MetaData->Vps);
			MetaData->Vps = NULL;
		}

		if(MetaData->Sps != NULL){
			printf("Free Sps\n");
			free(MetaData->Sps);
			MetaData->Sps = NULL;
		}
		printf("Free MetaData\n");
		free(MetaData);
		MetaData = NULL;
	}
#endif

	return;
}

int SendAMFPacket(RTMP *rtmp, LPRTMPMetadata lpMetaData)
{
	if(lpMetaData == NULL)
	{
		return -1;
	}
	char body[1024] = {0};;

	char * p = (char *)body;
	p = put_byte(p, AMF_STRING );
	p = put_amf_string(p , "@setDataFrame" );

	p = put_byte( p, AMF_STRING );
	p = put_amf_string( p, "onMetaData" );

	//p = put_byte(p, AMF_ECMA_ARRAY );
	//p = put_be32(p, 5 );
	p = put_byte(p, AMF_OBJECT );
	p = put_amf_string( p, "copyright" );
	p = put_byte(p, AMF_STRING );
	p = put_amf_string( p, "firehood" );

	p =put_amf_string( p, "duration");
	p =put_amf_double( p, 0);

	p =put_amf_string( p, "fileSize");
	p =put_amf_double( p, 0);

	p =put_amf_string( p, "width");
	p =put_amf_double( p, lpMetaData->nWidth);

	p =put_amf_string( p, "height");
	p =put_amf_double( p, lpMetaData->nHeight);

	p =put_amf_string( p, "framerate" );
	p =put_amf_double( p, lpMetaData->nFrameRate);

	//p =put_amf_string( p, "videocodecid" );
	//p =put_byte( p, 0x02);
	//p =put_amf_string( p, "avc1" );

	p =put_amf_string( p, "videocodecid" );
	p =put_amf_double( p, FLV_CODECID_H264 );

	//p =put_amf_string( p, "videodatarate" );
	//p =put_amf_double( p, 2500 );
	//p =put_amf_double( p, FLV_CODECID_H264 );

	p =put_amf_string( p, "" );
	p =put_byte( p, AMF_OBJECT_END  );

	// int index = p-body;

	return SendPacket(rtmp, RTMP_PACKET_TYPE_INFO,(unsigned char*)body,p-body,0);

}

int SendPacket(RTMP *rtmp, unsigned int nTagType,unsigned char *data,unsigned int size,unsigned int nTimestamp)
{
	RTMPPacket* packet;

	packet = (RTMPPacket *)malloc(RTMP_HEAD_SIZE+size);
	if (!packet) {
		printf("SendPacket: Can not allocate memory\n");
		return -1;
	}
	memset(packet,0,RTMP_HEAD_SIZE);

	packet->m_body = (char *)packet + RTMP_HEAD_SIZE;
	packet->m_nBodySize = size;
	memcpy(packet->m_body,data,size);
	packet->m_hasAbsTimestamp = 0;
	packet->m_packetType = nTagType;
	packet->m_nInfoField2 = rtmp->m_stream_id;
	packet->m_nChannel = 0x04;

	packet->m_headerType = RTMP_PACKET_SIZE_LARGE;
	if (RTMP_PACKET_TYPE_AUDIO ==nTagType && size !=4)
	{
		packet->m_headerType = RTMP_PACKET_SIZE_MEDIUM;
	}
	packet->m_nTimeStamp = nTimestamp;

	int nRet =0;
	if ((rtmp != NULL) && RTMP_IsConnected(rtmp))
	{
		pthread_mutex_lock(&rtmp_sync);
		nRet = RTMP_SendPacket(rtmp, packet, TRUE);
		pthread_mutex_unlock(&rtmp_sync);
	}

	// if(packet->m_body){
	// 	free(packet->m_body);
	// }
	free(packet);
	return nRet;
}


int SendVideoSpsPpsPacket(RTMP *rtmp, unsigned char *pps,int pps_len,unsigned char * sps,int sps_len)
{
	int nRet = -1;
	RTMPPacket * packet=NULL;//rtmp���ṹ
	unsigned char * body=NULL;
	int i;
	if (rtmp == NULL || pps == NULL || sps == NULL || pps_len == 0 || sps_len == 0) {
		// printf("SendVideoSpsPpsPacket: Invalid Input\n");
		return -1;
	}
	packet = (RTMPPacket *)malloc(RTMP_HEAD_SIZE+1024);
	if (!packet){
		printf("SendVideoSpsPpsPacket: Can not allocate memory\n");
		return nRet;
	}
	//RTMPPacket_Reset(packet);//����packet״̬
	memset(packet,0,RTMP_HEAD_SIZE+1024);
	packet->m_body = (char *)packet + RTMP_HEAD_SIZE;
	body = (unsigned char *)packet->m_body;
	i = 0;
	body[i++] = 0x17;
	body[i++] = 0x00;

	body[i++] = 0x00;
	body[i++] = 0x00;
	body[i++] = 0x00;

	/*AVCDecoderConfigurationRecord*/
	body[i++] = 0x01;
	body[i++] = sps[1];
	body[i++] = sps[2];
	body[i++] = sps[3];
	body[i++] = 0xff;

	/*sps*/
	body[i++]   = 0xe1;
	body[i++] = (sps_len >> 8) & 0xff;
	body[i++] = sps_len & 0xff;
	memcpy(&body[i],sps,sps_len);
	i +=  sps_len;

	/*pps*/
	body[i++]   = 0x01;
	body[i++] = (pps_len >> 8) & 0xff;
	body[i++] = (pps_len) & 0xff;
	memcpy(&body[i],pps,pps_len);
	i +=  pps_len;

	packet->m_packetType = RTMP_PACKET_TYPE_VIDEO;
	packet->m_nBodySize = i;
	packet->m_nChannel = 0x04;
	packet->m_nTimeStamp = 0;//tick;
	packet->m_hasAbsTimestamp = 0;
	packet->m_headerType = RTMP_PACKET_SIZE_MEDIUM;
	packet->m_nInfoField2 = rtmp->m_stream_id;

	if ((rtmp != NULL) && RTMP_IsConnected(rtmp))
	{
		pthread_mutex_lock(&rtmp_sync);
		nRet = RTMP_SendPacket(rtmp, packet, TRUE);
		pthread_mutex_unlock(&rtmp_sync);
	}

	free(packet);
	return nRet;
}

int SendH264Packet(RTMP *rtmp, LPRTMPMetadata lpMetaData, unsigned char *data,unsigned int size,int bIsKeyFrame,unsigned int nTimeStamp)
{
	int ret = -1;
	if(data == NULL && size<11)
	{
		return -1;
	}

	unsigned char *body = (unsigned char*)malloc(size+9);
	if (!body){
		printf("SendH264Packet: Can not allocate memory\n");
		return ret;
	}
	memset(body,0,size+9);

	int i = 0;
	if(bIsKeyFrame)
	{
		body[i++] = 0x17;// 1:Iframe  7:AVC
		body[i++] = 0x01;// AVC NALU
		body[i++] = 0x00;
		body[i++] = 0x00;
		body[i++] = 0x00;

		// NALU size
		body[i++] = size>>24 &0xff;
		body[i++] = size>>16 &0xff;
		body[i++] = size>>8 &0xff;
		body[i++] = size&0xff;
		// NALU data
		memcpy(&body[i],data,size);

		//if send header fail, resend header.
		// if (lpMetaData->is_send_header_h264 == false){
		// 	printf ("Send header h264\n");
		// 	ret = SendVideoSpsPpsPacket(rtmp, lpMetaData->Pps,lpMetaData->nPpsLen,lpMetaData->Sps,lpMetaData->nSpsLen);
		// 	if (ret == 1){
		// 		lpMetaData->is_send_header_h264 = true;
		// 	}
		// }
	}
	else
	{
		body[i++] = 0x27;// 2:Pframe  7:AVC
		body[i++] = 0x01;// AVC NALU
		body[i++] = 0x00;
		body[i++] = 0x00;
		body[i++] = 0x00;

		// NALU size
		body[i++] = size>>24 &0xff;
		body[i++] = size>>16 &0xff;
		body[i++] = size>>8 &0xff;
		body[i++] = size&0xff;
		// NALU data
		memcpy(&body[i],data,size);
	}

	int bRet = SendPacket(rtmp, RTMP_PACKET_TYPE_VIDEO,body,i+size,nTimeStamp);

	free(body);

	return bRet;
}

int SendH264File(RTMP *rtmp, LPRTMPMetadata lpMetaData, char *filename)
{
	unsigned char *h264FileBuffer;
	int h264FileLength;

	// int hLength=0;
	FILE *fp = NULL;
	if (filename == NULL)
	{
		DebugPrint("%s is not error", filename);
		return -1;
	}
	fp = fopen(filename, "rb");
	if (fp == NULL)
	{
		DebugPrint("%s is not exist", filename);
		return -1;
	}
	fseek(fp, 0, SEEK_END);
	h264FileLength = ftell(fp);
	h264FileBuffer = (unsigned char *)malloc(h264FileLength);
	if (!h264FileBuffer){
		printf("SendH264File: Can not allocate memory\n");
		return -1;
	}
	fseek(fp, 0, SEEK_SET);
	fread(h264FileBuffer, 1, h264FileLength, fp);
	fclose(fp);

	Geth264FileSpsPpsData(h264FileBuffer, h264FileLength, lpMetaData);
	//parseSpsData();//����sps��ȡ�����߼�������Ϣ
	//h264_decode_sps(lpMetaData->Sps, lpMetaData->nSpsLen, &lpMetaData->nWidth,  &lpMetaData->nHeight, &lpMetaData->nFrameRate);
	lpMetaData->nWidth = 1280;
	lpMetaData->nHeight = 720;
	lpMetaData->nFrameRate = 25;
	DebugPrint("Width : %d; Heigth %d; FrameRate %d", lpMetaData->nWidth, lpMetaData->nHeight, lpMetaData->nFrameRate);
	if (lpMetaData->nFrameRate==0)
	{
		lpMetaData->nFrameRate = 25;
	}

	unsigned int tick = 0;
	unsigned int tick_gap = 1000/lpMetaData->nFrameRate;
	int m_CurPos = 0;
	//unsigned char *p = h264FileBuffer;
	NaluUnit nalu;
	int readSize = 0;
	while (m_CurPos<h264FileLength)
	{
		unsigned char *p = h264FileBuffer+m_CurPos;
		readSize = ReadOneNaluFromBuf(&nalu, p, h264FileLength-m_CurPos);
		m_CurPos += readSize;
		if (nalu.type == 0x07||nalu.type == 0x08)
		{
			continue;
		}
		int bKeyframe  = (nalu.type == 0x05) ? TRUE : FALSE;
		SendH264Packet(rtmp, lpMetaData, nalu.data, nalu.size, bKeyframe, tick);
		tick += tick_gap;
		if (nalu.data != NULL)
		{
			free(nalu.data);
		}
		DebugPrint("m_Cur Pos : %d, tick : %d", m_CurPos, tick);
		usleep(tick_gap);
	}
	return 0;
}

int Geth264FileSpsPpsData(unsigned char *fileData, int fileLength, LPRTMPMetadata pMetaData)
{
	if (fileData == NULL)
	{
		DebugPrint("data is null");
		return -1;
	}
	int readNaluType=0;//���û�ȡ��־
	int m_CurPos=0;
	int i = 0;
	unsigned char *p = fileData;
	while (i<fileLength)
	{
		int j = i;
		if (p[j++]==0x00&&p[j++]==0x00)
		{
			if ((p[j]==0x01) || (p[j++]==0x00 && p[j]==0x01))
			{
				j++;
				if ((p[j]&0x1f)==NALU_TYPE_SPS||readNaluType==NALU_TYPE_SPS)//sps
				{
					if (readNaluType ==0 )
					{
						readNaluType = NALU_TYPE_SPS;
						m_CurPos = j;//��ȡsps��λ��
						i = j;
						continue;
					}
					else if (readNaluType == NALU_TYPE_SPS)
					{
						//��������
						pMetaData->nSpsLen = i-m_CurPos;
						pMetaData->Sps = (unsigned char *)malloc(pMetaData->nSpsLen);
						if (!pMetaData->Sps){
							printf("Geth264FileSpsPpsData: Can not allocate memory\n");
							return -1;
						}
						memcpy(pMetaData->Sps, &p[m_CurPos], pMetaData->nSpsLen);
						m_CurPos = 0;
						readNaluType = 0;
						i--;
					}
				}
				else if ((p[j]&0x1f)==NALU_TYPE_PPS||readNaluType==NALU_TYPE_PPS)//pps
				{
					if (readNaluType == 0 )
					{
						readNaluType = NALU_TYPE_PPS;
						m_CurPos = j;//��ȡsps��λ��
						i = j;
						continue;
					}
					else if (readNaluType == NALU_TYPE_PPS)
					{
						//��������
						pMetaData->nPpsLen = i-m_CurPos;
						pMetaData->Pps = (unsigned char *)malloc(pMetaData->nPpsLen);
						if (!pMetaData->Pps){
							printf("Geth264FileSpsPpsData: 1.Can not allocate memory\n");
							return -1;
						}
						memcpy(pMetaData->Pps, &p[m_CurPos], pMetaData->nPpsLen);
						m_CurPos = 0;
						readNaluType = 0;
						return 0;
					}
				}
			}
		}
		i++;
	}

	return 0;

}

int ReadOneNaluFromBuf(NaluUnit *nalu, unsigned char *data, int dataLength)
{
	int i = 0;
	while(i<dataLength)
	{
		int j = i;
		if(data[j++] == 0x00 &&  data[j++] == 0x00)
		{
			if ((data[j] == 0x01) || (data[j++] == 0x00 &&  data[j] == 0x01))
			{
				j++;
				int m_CurPos = j;
				nalu->type = data[j]&0x1f;
				while (j<dataLength)
				{
					int k = j;
					if (data[k++] == 0x00 &&  data[k++] == 0x00)
					{
						if ((data[k] == 0x01)||(data[k++] == 0x00 &&  data[k] == 0x01))
						{
							nalu->size = j - m_CurPos;
							nalu->data = (unsigned char *)malloc(nalu->size);
							if (!nalu->data){
								printf("ReadOneNaluFromBuf: Can not allocate memory\n");
								return -1;
							}
							memcpy(nalu->data, data+m_CurPos, nalu->size);
							return j;
						}
					}
					j++;
				}
				nalu->size = j - m_CurPos;
				nalu->data = (unsigned char *)malloc(nalu->size);
				if (!nalu->data){
					printf("ReadOneNaluFromBuf: 1.Can not allocate memory\n");
					return -1;
				}
				memcpy(nalu->data, data+m_CurPos, nalu->size);
				return j;//���һ��Nalu�Ĵ���
			}
		}
		i++;
	}
	return 0;

}

int is_same_data(unsigned char *s1, unsigned char *s2, int len) {
	int ret = 1;
	int i;
	for(i=0;i<len;i++) {
		if(s1[i] != s2[i]) {
			ret = 0;
			break;
		}
	}
	return ret;
}

int getMetaDataH264(unsigned char *h264Buffer, unsigned int h264size)
{
	NaluUnit nalu;
	int is_update = 0;

	nalu.data = h264Buffer + 4;
	nalu.size = h264size-4;

	if (MetaData == NULL){
		MetaData = (RTMPMetadata*)malloc(sizeof(RTMPMetadata));
		if (MetaData == NULL) {
			printf("getMetaDataH264: can not malloc MetaData\n");
			return -1;
		}
		memset(MetaData, 0, sizeof(RTMPMetadata));
		MetaData->Pps = NULL;
		MetaData->Sps = NULL;
		MetaData->Vps = NULL;
	}

	nalu.type = nalu.data[0]&0x1f;
	if (nalu.type == 0x07||nalu.type == 0x08)
	{
		is_update = 0;
		if (nalu.type == 0x07)
		{
			if(nalu.size != MetaData->nSpsLen) {
				is_update = 1;
			} else {
				// compare and update
				if(is_same_data(MetaData->Sps, nalu.data, MetaData->nSpsLen)) {
					// same sps data
				} else {
					is_update = 1;
				}
			}

			if(is_update) {
				MetaData->nSpsLen = nalu.size;
				if(MetaData->Sps) {
					printf("already malloc sps\n");
					free(MetaData->Sps);
				}
				// else
				{
					MetaData->Sps = (unsigned char *)malloc(MetaData->nSpsLen);
					if (!MetaData->Sps){
						printf("getMetaDataH264: Can not allocate memory\n");
						return -1;
					}
					printf("1st malloc sps\n");
				}
				memcpy(MetaData->Sps, nalu.data, MetaData->nSpsLen);
				printf("update new sps data\n");
				// MetaData->is_send_header_aac = false;
				// MetaData.is_send_header_h264 = false;
			}
		}
		if (nalu.type == 0x08)
		{
			is_update = 0;
			if(nalu.size != MetaData->nPpsLen) {
				is_update = 1;
			} else {
				// compare and update
				if(is_same_data(MetaData->Pps, nalu.data, MetaData->nPpsLen)) {
					// same pps data
				} else {
					is_update = 1;
				}
			}

			if(is_update) {
				MetaData->nPpsLen = nalu.size;
				if(MetaData->Pps) {
					printf("already malloc pps\n");
					free(MetaData->Pps);
				}
				// else
				{
					MetaData->Pps = (unsigned char *)malloc(MetaData->nPpsLen);
					if (!MetaData->Pps){
						printf("getMetaDataH264: Can not allocate memory\n");
						return -1;
					}
					printf("1st malloc pps\n");
				}
				memcpy(MetaData->Pps, nalu.data, MetaData->nPpsLen);
				printf("update new pps data\n");
				MetaData->is_send_header_aac = false;
				// MetaData.is_send_header_h264 = false;
			}
		}
		return 0;
	}
	return 1;
}

int putH264BufferToRtmpStream(unsigned char *h264Buffer, unsigned int h264size)
{
	int ret = -1;
	if(g_rtmp == NULL) {
		return ret;
	}

	NaluUnit nalu;

	nalu.data = h264Buffer + 4;
	nalu.size = h264size-4;

	nalu.type = nalu.data[0]&0x1f;
	// printf("nalu.type=0x%02X\n", nalu.type);
#if 1
	if (MetaData->is_send_header_h264 == false && MetaData->Pps != NULL && MetaData->Sps != NULL){
		printf ("Send header h264\n");
		ret = SendVideoSpsPpsPacket(g_rtmp, MetaData->Pps,MetaData->nPpsLen,MetaData->Sps,MetaData->nSpsLen);
		if (ret == 1){
			MetaData->is_send_header_h264 = true;
		}
	}
#endif

#if 1
	if (nalu.type == 0x07||nalu.type == 0x08)
	{
		getMetaDataH264(h264Buffer, h264size);
		if (/* MetaData->is_send_header_h264 == false && */ (nalu.type == 0x08))
		// if (nalu.type == 0x08)
		{
			printf ("1.Send header h264\n");
			ret = SendVideoSpsPpsPacket(g_rtmp, MetaData->Pps,MetaData->nPpsLen,MetaData->Sps,MetaData->nSpsLen);
			if (ret == 1){
				MetaData->is_send_header_h264 = true;
			}
		}
		return 2;
	}
#endif

	if(MetaData->nSpsLen == 0 || MetaData->nPpsLen == 0 || !MetaData->is_send_header_h264){
		return 2;
	}
	// printf("nalu.type : %d\n", nalu.type);
	int bKeyframe  = (nalu.type == 0x05) ? TRUE : FALSE;

	if (tick_gap == 0){
		tick_gap = RTMP_GetTime();
	}else{
		tick_video = RTMP_GetTime() - tick_gap;
	}

	// if(tick > 0xffffff){
	// 	printf("Reset timestamp1\n");
	// 	return 0;
	// }

	ret = SendH264Packet(g_rtmp, MetaData, nalu.data, nalu.size, bKeyframe, tick_video);
	if(ret == -1 || ret == 0){
		printf("Send data h264 to rtmp fail: %d\n", ret);
	}

	return ret;
}

//H265
/**
 * Send the sps and pps, vps information of the video
 *
 * @param pps stores the pps information of the video
 * @param pps_len video pps information length
 * @param sps stores the pps information of the video
 * @param sps_len video sps information length
 * @param vps stores the vps information of the video
 * @param vps_len video vps information length
 *
 * @Returns 1 if it succeeds, 0 if it fails
 */
int SendVideoSpsPps(RTMP *rtmp, unsigned char *pps,int pps_len,unsigned char *sps,int sps_len,unsigned char* vps, int vps_len)
{
	RTMPPacket * packet=NULL;//rtmp packet structure
	unsigned char * body=NULL;
	int i = 0;
	packet = (RTMPPacket *)malloc(RTMP_HEAD_SIZE+1024);
	if (!packet){
		printf("SendVideoSpsPps: Can not allocate memory\n");
		return -1;
	}
	//RTMPPacket_Reset(packet);//Reset packet state
	memset(packet,0,RTMP_HEAD_SIZE+1024);
	packet->m_body = (char *)packet + RTMP_HEAD_SIZE;
	body = (unsigned char *)packet->m_body;

	body[i++] = 0x1C;
	body[i++] = 0x00;
	body[i++] = 0x00;
	body[i++] = 0x00;
	body[i++] = 0x00;
	body[i++] = 0x00;

	//general_profile_idc 8bit
	body[i++] = sps[1];
	//general_profile_compatibility_flags 32 bit
	body[i++] = sps[2];
	body[i++] = sps[3];
	body[i++] = sps[4];
	body[i++] = sps[5];

	//48 bit NUll nothing deal in rtmp
	body[i++] = sps[6];
	body[i++] = sps[7];
	body[i++] = sps[8];
	body[i++] = sps[9];
	body[i++] = sps[10];
	body[i++] = sps[11];

	//general_level_idc
	body[i++] = sps[12];

	//48 bit NUll nothing deal in rtmp
	body[i++] = 0x00;
	body[i++] = 0x00;
	body[i++] = 0x00;
	body[i++] = 0x00;
	body[i++] = 0x00;
	body[i++] = 0x00;

	//bit(16) avgFrameRate;
	body[i++] = 0x00;
	body[i++] = 0x00;

	/* bit(2) constantFrameRate; */
	/* bit(3) numTemporalLayers; */
	/* bit(1) temporalIdNested; */
	body[i++] = 0x00;

	/* unsigned int(8) numOfArrays; 03 */
	body[i++] = 0x03;

	printf("HEVCDecoderConfigurationRecord data = %s\n", body);
	body[i++] = 0x20;//vps 32
	body[i++] = (1 >> 8) & 0xff;
	body[i++] = 1 & 0xff;
	body[i++] = (vps_len >> 8) & 0xff;
	body[i++] = (vps_len) & 0xff;
	memcpy(&body[i], vps,
	 vps_len);
	i += vps_len;

	//sps
	body[i++] = 0x21;//sps 33
	body[i++] = (1 >> 8) & 0xff;
	body[i++] = 1 & 0xff;
	body[i++] = (sps_len >> 8) & 0xff;
	body[i++] = sps_len & 0xff;
	memcpy(&body[i], sps, sps_len);
	i += sps_len;

	//pps
	body[i++] = 0x22;//pps 34
	body[i++] = (1 >> 8) & 0xff;
	body[i++] = 1 & 0xff;
	body[i++] = (pps_len >> 8) & 0xff;
	body[i++] = (pps_len) & 0xff;
	memcpy(&body[i], pps, pps_len);
	i += pps_len;

	packet->m_packetType = RTMP_PACKET_TYPE_VIDEO;
	packet->m_nBodySize = i;
	packet->m_nChannel = 0x04;
	packet->m_nTimeStamp = 0;
	packet->m_hasAbsTimestamp = 0;
	packet->m_headerType = RTMP_PACKET_SIZE_MEDIUM;
	packet->m_nInfoField2 = rtmp->m_stream_id;

	/*Call the sending interface*/
	int nRet = RTMP_SendPacket(rtmp,packet,TRUE);
	free(packet);//Release memory
	return nRet;
}

/**
 * Send H265 data frame
 *
 * @param data stores the content of the data frame
 * @param size The size of the data frame
 * @param bIsKeyFrame records whether the frame is a key frame
 * @param nTimeStamp The timestamp of the current frame
 *
 * @Returns 1 if it succeeds, 0 if it fails
 */

int SendH265Packet(RTMP *rtmp, LPRTMPMetadata lpMetaData, unsigned char *data,unsigned int size,int bIsKeyFrame,unsigned int nTimeStamp)
{
	if(data == NULL && size<11){
		return -1;
	}

	unsigned char *body = (unsigned char*)malloc(size+9);
	if (!body){
		printf("SendH265Packet: Can not allocate memory\n");
		return -1;
	}
	memset(body,0,size+9);

	int i = 0;
	if(bIsKeyFrame){
		body[i++] = 0x1C;//1:Iframe  12:HEVC
		SendVideoSpsPps(rtmp, lpMetaData->Pps, lpMetaData->nPpsLen, lpMetaData->Sps,lpMetaData->nSpsLen, lpMetaData->Vps,lpMetaData->nVpsLen);
	}else{
		body[i++] = 0x2C;//2:Pframe 7:AVC
	}

	body[i++] = 0x01;// HEVC NALU
	body[i++] = 0x00;
	body[i++] = 0x00;
	body[i++] = 0x00;

	//NALU size
	body[i++] = size >> 24 & 0xff;
	body[i++] = size >> 16 & 0xff;
	body[i++] = size >> 8 & 0xff;
	body[i++] = size & 0xff;
	//NALU data
	memcpy(&body[i], data, size);

	int bRet = SendPacket(rtmp, RTMP_PACKET_TYPE_VIDEO,body,i+size,nTimeStamp);

	free(body);

	return bRet;
}

int putH265BufferToRtmpStream(RTMP *rtmp, LPRTMPMetadata lpMetaData, unsigned char *h265Buffer, unsigned int h265size)
{
	NaluUnit nalu;

	nalu.data = h265Buffer + 4;
	nalu.size = h265size - 4;

	nalu.type = (nalu.data[0] >> 1) & 0x3f;

	printf("nalu.type : %2x\n", nalu.type);

	if (nalu.type == 0x20 || nalu.type == 0x21 || nalu.type == 0x22)
	{
		if (nalu.type == 0x20 && lpMetaData->nSpsLen == 0)
		{
			lpMetaData->nSpsLen = nalu.size;
			lpMetaData->Sps = (unsigned char *)malloc(lpMetaData->nSpsLen);
			if (!lpMetaData->Sps){
				printf("putH265BufferToRtmpStream: SPS Can not allocate memory\n");
				return -1;
			}
			memcpy(lpMetaData->Sps, nalu.data, lpMetaData->nSpsLen);
			DebugPrint("Update sps\n");
		}

		if (nalu.type == 0x21 && lpMetaData->nVpsLen == 0)
		{
			lpMetaData->nVpsLen = nalu.size;
			lpMetaData->Vps = (unsigned char *)malloc(lpMetaData->nVpsLen);
			if (!lpMetaData->Vps){
				printf("putH265BufferToRtmpStream: VPS Can not allocate memory\n");
				return -1;
			}
			memcpy(lpMetaData->Vps, nalu.data, lpMetaData->nVpsLen);
			DebugPrint("Update vps");
		}

		if (nalu.type == 0x22 && lpMetaData->nPpsLen == 0)
		{
			lpMetaData->nPpsLen = nalu.size;
			lpMetaData->Pps = (unsigned char *)malloc(lpMetaData->nPpsLen);
			if (!lpMetaData->Pps){
				printf("putH265BufferToRtmpStream: PPS Can not allocate memory\n");
				return -1;
			}
			memcpy(lpMetaData->Pps, nalu.data, lpMetaData->nPpsLen);
			DebugPrint("Update pps");
		}

		return 0;
	}
	int bKeyframe  = (nalu.type == 0x13) ? TRUE : FALSE;
	tick_video = RTMP_GetTime();
	SendH265Packet(rtmp, lpMetaData, nalu.data, nalu.size, bKeyframe, tick_video);

	return 0;
}

int AccGetDecoderSpecificInfo(unsigned char *aac_buffer)
{
	int aac_sdp_config = 0;
	ADTS_2 pdts2;
	memcpy(&pdts2, &aac_buffer[2], 1);
	ADTS_3 pdts3;
	memcpy(&pdts3, &aac_buffer[3], 1);
	unsigned char aac_audio_object_type =
		(unsigned char)((0x00 | pdts2.profile) + 1);
	unsigned char aac_frequency_index =
		(unsigned char)(0x00 | pdts2.sample_freqency_index);
	unsigned char aac_channel_conf =
		(unsigned char)(0x00 | ((pdts2.channel_conf_3 << 2) |
								pdts3.channel_conf_2to1));
	unsigned char config0 =
		((aac_audio_object_type << 3) | (aac_frequency_index >> 1));
	unsigned char config1 =
		((aac_frequency_index << 7) | (aac_channel_conf << 3));

	aac_sdp_config = ((config0 << 8) | config1);
	printf("Audio aac sdp_config = 0x%04x,audio_type = %d  frequency_index = "
		   "%d channel =%d\n",
		   aac_sdp_config, aac_audio_object_type,
		   aac_frequency_index, aac_channel_conf);

	return aac_sdp_config;
}

//Audio AAC
int rtmp_send_aac_spec(RTMP *rtmp, unsigned char *buf,int len)
{
	int ret = -1;
	if(tick_gap == 0){
		tick_gap = RTMP_GetTime();
	}else {
		tick_audio = RTMP_GetTime() - tick_gap;
	}

	// if(tick > 0xffffff){
	// 	printf("Reset timestamp2\n");
	// 	return 0;
	// }

    RTMPPacket * packet;
    unsigned char * body;
	int aac_config = 0;
	uint8_t spec_buf[2] = {0};

	aac_config = AccGetDecoderSpecificInfo(buf);

	spec_buf[0] = (aac_config >> 8) & 0xFF;
	spec_buf[1] = aac_config & 0xFF;

    packet = (RTMPPacket *)malloc(RTMP_HEAD_SIZE+len+2);
	if (!packet){
		printf("rtmp_send_aac_spec: Can not allocate memory\n");
		return -1;
	}
    memset(packet,0,RTMP_HEAD_SIZE);

    packet->m_body = (char *)packet + RTMP_HEAD_SIZE;
    body = (unsigned char *)packet->m_body;

    /*AF 00 + AAC RAW data*/
	//AAC, sample rate 8K, 16-bit samples, Mono.
    body[0] = 0xA6;//0xAF;
    body[1] = 0x00;
    memcpy(&body[2],spec_buf,sizeof(spec_buf)); /*spec_buf是AAC sequence header数据*/

    packet->m_packetType = RTMP_PACKET_TYPE_AUDIO;
    packet->m_nBodySize = len+2;
    packet->m_nChannel = 0x04;
    packet->m_nTimeStamp = 0;//tick;
    packet->m_hasAbsTimestamp = 0;
    packet->m_headerType = RTMP_PACKET_SIZE_LARGE;//RTMP_PACKET_SIZE_LARGE;
    packet->m_nInfoField2 = rtmp->m_stream_id;

    /*调用发送接口*/
	if (RTMP_IsConnected(rtmp)){
		pthread_mutex_lock(&rtmp_sync);
    	ret = RTMP_SendPacket(rtmp,packet,TRUE);
		pthread_mutex_unlock(&rtmp_sync);
	}

	free(packet);
    return ret;
}

//1024/8
int rtmp_send_raw_aac(RTMP *rtmp, unsigned char *buf,int len)
{
	int ret = -1;
	// if(tick_gap == 0){
	// 	tick_gap = RTMP_GetTime();
	// }else {
	// 	tick_audio = RTMP_GetTime() - tick_gap;
	// }

	// if(tick > 0xffffff){
	// 	printf("Reset timestamp\n");
	// 	return 0;
	// }

	tick_audio += 1024/16; //16k

    buf += 7;
    len -= 7;

    if (len > 0) {
        RTMPPacket * packet = NULL;
        unsigned char * body;

        packet = (RTMPPacket *)malloc(RTMP_HEAD_SIZE+len+2);
		if(!packet) {
			printf("rtmp_send_raw_aac: Can not allocate memory\n");
			return -1;

		}
        memset(packet,0,RTMP_HEAD_SIZE);

        packet->m_body = (char *)packet + RTMP_HEAD_SIZE;
        body = (unsigned char *)packet->m_body;

        /*AF 01 + AAC RAW data*/
        body[0] = 0xA6;//0xAF;
        body[1] = 0x01;
        memcpy(&body[2],buf,len);

        packet->m_packetType = RTMP_PACKET_TYPE_AUDIO;
        packet->m_nBodySize = len+2;
        packet->m_nChannel = 0x04;
        packet->m_nTimeStamp = tick_audio;
        packet->m_hasAbsTimestamp = 0;
        packet->m_headerType = RTMP_PACKET_SIZE_MEDIUM;
        packet->m_nInfoField2 = rtmp->m_stream_id;

        /*调用发送接口*/
		if (RTMP_IsConnected(rtmp)){
			pthread_mutex_lock(&rtmp_sync);
			ret = RTMP_SendPacket(rtmp,packet,TRUE);
			pthread_mutex_unlock(&rtmp_sync);
		}
        free(packet);
    }
    return ret;
}

int rtmp_send_aac(unsigned char *buf,int len)
{
	int ret = -1;
	if(g_rtmp == NULL) {
		return ret;
	}

	if(MetaData->is_send_header_aac == false){
		MetaData->is_send_header_aac = true;
		ret = rtmp_send_aac_spec(g_rtmp, buf, len);
		if(ret == 0){
			printf("Send header aac to rtmp fail: %d\n", ret);
			return ret;
		}
	}
	ret = rtmp_send_raw_aac(g_rtmp, buf, len);

	return ret;
}

//Audio G711a
void sendg711a_audio(RTMP *rtmp, unsigned char *buf,int len, int timeStamp)
{
    if (len > 0) {
        RTMPPacket * packet = NULL;
        unsigned char * body;

		packet = (RTMPPacket *)malloc(RTMP_HEAD_SIZE+1024);
		if(!packet){
			printf("sendg711a_audio: Can not allocate memory\n");
			return;
		}
        memset(packet,0,RTMP_HEAD_SIZE);

        packet->m_body = (char *)packet + RTMP_HEAD_SIZE;
        body = (unsigned char *)packet->m_body;

        body[0] = 0x76;//Mono channel, 16bit sampling accuracy, 8K sampling frequency
        memcpy(&body[1],buf,len);

        packet->m_packetType = RTMP_PACKET_TYPE_AUDIO;
        packet->m_nBodySize = len+1;
        packet->m_nChannel = 0x04;
        packet->m_nTimeStamp = timeStamp;
        packet->m_hasAbsTimestamp = 0;
        packet->m_headerType = RTMP_PACKET_SIZE_MEDIUM;
        packet->m_nInfoField2 = rtmp->m_stream_id;

		if (RTMP_IsConnected(rtmp)){
			pthread_mutex_lock(&rtmp_sync);
			RTMP_SendPacket(rtmp,packet,TRUE);
			pthread_mutex_unlock(&rtmp_sync);
		}

        free(packet);
    }
    return;
}