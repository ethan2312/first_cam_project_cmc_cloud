# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "C"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_C
  "/home/phamtuan/Documents/project/rtmp/rtmp/src/Common.c" "/home/phamtuan/Documents/project/rtmp/rtmp/CMakeFiles/rtmp.dir/src/Common.c.o"
  "/home/phamtuan/Documents/project/rtmp/rtmp/src/capture_and_encoding.c" "/home/phamtuan/Documents/project/rtmp/rtmp/CMakeFiles/rtmp.dir/src/capture_and_encoding.c.o"
  "/home/phamtuan/Documents/project/rtmp/rtmp/src/imp-common.c" "/home/phamtuan/Documents/project/rtmp/rtmp/CMakeFiles/rtmp.dir/src/imp-common.c.o"
  "/home/phamtuan/Documents/project/rtmp/rtmp/src/main.c" "/home/phamtuan/Documents/project/rtmp/rtmp/CMakeFiles/rtmp.dir/src/main.c.o"
  "/home/phamtuan/Documents/project/rtmp/rtmp/src/rtmpStream.c" "/home/phamtuan/Documents/project/rtmp/rtmp/CMakeFiles/rtmp.dir/src/rtmpStream.c.o"
  )
set(CMAKE_C_COMPILER_ID "GNU")

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "./include"
  "./include/imp_sys"
  "./include/imp_sys/imp"
  "./include/imp_sys/sysutils"
  "./src"
  "./lib"
  "./lib/rtmp/librtmp"
  "./lib/rtmp/librtmp/include"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
